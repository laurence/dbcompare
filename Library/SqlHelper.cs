﻿using Dos.ORM;

namespace Dasic.Library
{
    public static class SqlHelper
    {
        public static readonly DbSession SqLiteConnection = new DbSession("SqliteConnection");

        public static DbSession MySqlConnection(string str)
        {
            return new DbSession(DatabaseType.MySql, str);
        }
    }
}